import fs from 'fs';
import { FileWriter } from '../src/fileWriter';
import { Listener } from '../src/l3WebSocketServer';
import { OrderBook } from '../src/orderBook';
import { readFileSync } from 'fs';
import WebSocket from 'ws';
import { RawOrder } from '../src/messageMapper';

const orderBook = new OrderBook();
const testFileName: string = './__mocks__/l1-test.csv';

if (fs.existsSync(testFileName)) {
  fs.unlinkSync(testFileName);
}

const eventToFileHandler = new FileWriter(testFileName);

orderBook.registerL1Listener(eventToFileHandler.eventHandler);
const webSocketServer = new Listener(10101, orderBook);

const l3Data: string = readFileSync('./__mocks__/l3-test-data.csv').toString();
const l3Lines = l3Data.split('\n');

beforeAll(() => jest.setTimeout(15 * 1000));

test('Doing a complete end 2 end test', (done: jest.DoneCallback) => {
  setTimeout(() => {
    const ws = new WebSocket('ws://localhost:10101');
    ws.on('open', function open() {
      l3Lines.forEach((line: string) => {
        const args: Array<any> = line.split(',');
        const order: RawOrder = {
          seq_number: args[0],
          add_order_id: args[1],
          add_side: args[2],
          add_price: Number(args[3]),
          add_qty: Number(args[4]),
          update_order_id: args[5],
          update_side: args[6],
          update_price: Number(args[7]),
          update_qty: Number(args[8]),
          delete_order_id: args[9],
          delete_side: args[10],
          trade_order_id: args[11],
          trade_side: args[12],
          trade_price: Number(args[13]) / 1000.0,
          trade_qty: Number(args[14]),
          time: args[15],
        };

        ws.send(JSON.stringify(order));
      });
      ws.close();
    });

    ws.on('close', () => {
      setTimeout(() => {
        eventToFileHandler.close();
        webSocketServer.close();
        const l1Mock: string = readFileSync('./__mocks__/l1-expected.csv').toString();
        const l1Test: string = readFileSync(testFileName).toString();
        expect(l1Test).toEqual(l1Mock);
        done();
      }, 1000);
    });
  }, 1000);
});

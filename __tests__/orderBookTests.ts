import { L1Data } from '../src/emitterInterface';
import { MarketOrder, MarketOrderSide, MarketOrderType } from '../src/marketOrder';
import { OrderBook } from '../src/orderBook';

const mock: Array<MarketOrder> = [
  {
    sequenceNumber: '1234562268920687408',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:04.159763 UTC',
    orderId: '7654321527799998336',
    price: 228.97,
    quantity: 1000,
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687410',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:05.32415 UTC',
    orderId: '7654321527799998330',
    price: 228.98,
    quantity: 1000,
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687411',
    orderType: MarketOrderType.DELETE,
    time: '1998-08-03 09:04:05.325476 UTC',
    orderId: '7654321527799998336',
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687412',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:05.325492 UTC',
    orderId: '7654321527799998336',
    price: 228.97,
    quantity: 1000,
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687413',
    orderType: MarketOrderType.DELETE,
    time: '1998-08-03 09:04:06.19658 UTC',
    orderId: '7654321527799998336',
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687414',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:06.1966 UTC',
    orderId: '7654321527799998336',
    price: 228.98,
    quantity: 1000,
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687416',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:21.994468 UTC',
    orderId: '7654321527799998335',
    price: 228.78,
    quantity: 1000,
    side: MarketOrderSide.BUY,
  },
  {
    sequenceNumber: '1234562268920687417',
    orderType: MarketOrderType.DELETE,
    time: '1998-08-03 09:04:21.994502 UTC',
    orderId: '7654321527799998336',
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687418',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:21.994518 UTC',
    orderId: '7654321527799998336',
    price: 228.97,
    quantity: 1000,
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687419',
    orderType: MarketOrderType.DELETE,
    time: '1998-08-03 09:04:21.995793 UTC',
    orderId: '7654321527799998335',
    side: MarketOrderSide.BUY,
  },
  {
    sequenceNumber: '1234562268920687420',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:21.995815 UTC',
    orderId: '7654321527799998335',
    price: 228.79,
    quantity: 1000,
    side: MarketOrderSide.BUY,
  },
  {
    sequenceNumber: '1234562268920687421',
    orderType: MarketOrderType.DELETE,
    time: '1998-08-03 09:04:26.083399 UTC',
    orderId: '7654321527799998335',
    side: MarketOrderSide.BUY,
  },
  {
    sequenceNumber: '1234562268920687422',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:26.083422 UTC',
    orderId: '7654321527799998335',
    price: 228.78,
    quantity: 1000,
    side: MarketOrderSide.BUY,
  },
  {
    sequenceNumber: '1234562268920687423',
    orderType: MarketOrderType.DELETE,
    time: '1998-08-03 09:04:26.407768 UTC',
    orderId: '7654321527799998335',
    side: MarketOrderSide.BUY,
  },
  {
    sequenceNumber: '1234562268920687424',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:26.407788 UTC',
    orderId: '7654321527799998335',
    price: 228.79,
    quantity: 1000,
    side: MarketOrderSide.BUY,
  },
  {
    sequenceNumber: '1234562268920687425',
    orderType: MarketOrderType.DELETE,
    time: '1998-08-03 09:04:28.595771 UTC',
    orderId: '7654321527799998336',
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687426',
    orderType: MarketOrderType.ADD,
    time: '1998-08-03 09:04:28.595792 UTC',
    orderId: '7654321527799998336',
    price: 228.98,
    quantity: 1000,
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687427',
    orderType: MarketOrderType.UPDATE,
    time: '1998-08-03 09:04:30.442001 UTC',
    orderId: '7654321527799998336',
    price: 228.79,
    quantity: 500,
    side: MarketOrderSide.SELL,
  },
  {
    sequenceNumber: '1234562268920687428',
    orderType: MarketOrderType.TRADE,
    time: '1998-08-03 09:04:30.441998 UTC',
    orderId: '7654321527799998336',
    price: 228.79,
    quantity: 200,
    side: MarketOrderSide.SELL,
  },
];

test('Create an empty Order book', function () {
  const orderBook = new OrderBook();

  expect(orderBook).toMatchSnapshot();
});

test('Add a sell order and a buy order to Order book', function () {
  const orderBook = new OrderBook();
  orderBook.addMarketOrder(mock[0]);
  orderBook.addMarketOrder(mock[6]);

  expect(orderBook).toMatchSnapshot();
});

test('Add a sell order and a buy order to Order book', function () {
  const orderBook = new OrderBook();
  orderBook.addMarketOrder(mock[0]);
  orderBook.addMarketOrder(mock[6]);

  expect(orderBook).toMatchSnapshot();
});

test('Add a sell order and a buy order then delete the sell order', function () {
  const orderBook = new OrderBook();
  orderBook.addMarketOrder(mock[0]);
  orderBook.addMarketOrder(mock[6]);
  orderBook.deleteMarketOrder(mock[2]);

  expect(orderBook).toMatchSnapshot();
});

test('Process the whole mock', function () {
  const orderBook = new OrderBook();
  const l1Messages: Array<L1Data> = [];
  const Listener = (data: L1Data) => l1Messages.push(data);
  orderBook.registerL1Listener(Listener);

  mock.forEach((order) => {
    orderBook.processMarketOrder(order);
    expect(orderBook).toMatchSnapshot();
    expect(l1Messages).toMatchSnapshot();
  });

  expect(l1Messages).toMatchSnapshot();
});

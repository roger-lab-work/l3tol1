import { PricePoint } from '../src/pricePoint';
import { MarketOrder, MarketOrderSide, MarketOrderType } from '../src/marketOrder';
import { traceDeprecation } from 'process';

const marketOrderMock1: MarketOrder = {
  sequenceNumber: '2342342',
  time: '1975-01-28 23:45',
  orderType: MarketOrderType.ADD,
  orderId: '3228935479283',
  side: MarketOrderSide.SELL,
  price: 123,
  quantity: 876,
};

const marketOrderMock2: MarketOrder = {
  sequenceNumber: '75467456',
  time: '2021-01-28 09:12',
  orderType: MarketOrderType.ADD,
  orderId: '23987235498',
  side: MarketOrderSide.SELL,
  price: 999,
  quantity: 1234,
};

const marketOrderDeleteMock: MarketOrder = {
  sequenceNumber: '56789',
  time: '1988-03-08 11:44',
  orderType: MarketOrderType.DELETE,
  orderId: '3228935479283',
  side: MarketOrderSide.SELL,
};

const marketOrderTradeMock: MarketOrder = {
  sequenceNumber: '456748456',
  time: '1988-03-08 11:12',
  orderType: MarketOrderType.TRADE,
  orderId: '99224834723',
  side: MarketOrderSide.BUY,
  quantity: 400,
};

test('Create an empty pricePoint', function () {
  const pricePoint = new PricePoint(123456);
  expect(pricePoint).toMatchSnapshot();
});

test('Add 2 sell orders to a PricePoint', function () {
  const pricePoint = new PricePoint(222);
  pricePoint.addMarketOrder(marketOrderMock1);
  pricePoint.addMarketOrder(marketOrderMock2);

  expect(pricePoint).toMatchSnapshot();
});

test('Add 2 buy orders to a PricePoint', function () {
  const pricePoint = new PricePoint(900);
  pricePoint.addMarketOrder({
    ...marketOrderMock1,
    side: MarketOrderSide.BUY,
  });

  pricePoint.addMarketOrder({
    ...marketOrderMock2,
    side: MarketOrderSide.BUY,
  });

  expect(pricePoint).toMatchSnapshot();
});

test('Add 2 sell orders and 1 buy order to a PricePoint', function () {
  const pricePoint = new PricePoint(987);
  pricePoint.addMarketOrder(marketOrderMock1);
  pricePoint.addMarketOrder(marketOrderMock2);
  pricePoint.addMarketOrder({
    ...marketOrderMock2,
    side: MarketOrderSide.BUY,
  });

  expect(pricePoint).toMatchSnapshot();
});

test('get Orders by orderId and MarkeOrderSide', function () {
  const pricePoint = new PricePoint(11);
  pricePoint.addMarketOrder(marketOrderMock1);
  pricePoint.addMarketOrder(marketOrderMock2);
  pricePoint.addMarketOrder({
    ...marketOrderMock2,
    side: MarketOrderSide.BUY,
  });

  expect(pricePoint.getOrder(marketOrderMock2.orderId, MarketOrderSide.SELL)).toBe(marketOrderMock2);
  expect(pricePoint.getOrder(marketOrderMock2.orderId, MarketOrderSide.BUY)).toEqual({
    ...marketOrderMock2,
    side: MarketOrderSide.BUY,
  });
});

test('Delete an order at a PricePoint', function () {
  const pricePoint = new PricePoint(1111);
  pricePoint.addMarketOrder(marketOrderMock1);
  pricePoint.addMarketOrder(marketOrderMock2);
  pricePoint.addMarketOrder({
    ...marketOrderMock1,
    side: MarketOrderSide.BUY,
  });
  pricePoint.deleteMarketOrder(marketOrderDeleteMock);

  expect(pricePoint.getOrder(marketOrderMock1.orderId, MarketOrderSide.SELL)).toBe(null);
  expect(pricePoint).toMatchSnapshot();
});

test('get total buy and sell quantity from a price point', function () {
  const pricePoint = new PricePoint(123);
  pricePoint.addMarketOrder(marketOrderMock1);
  pricePoint.addMarketOrder(marketOrderMock2);
  pricePoint.addMarketOrder({
    ...marketOrderMock1,
    side: MarketOrderSide.BUY,
    quantity: 56,
  });
  pricePoint.addMarketOrder({
    ...marketOrderMock2,
    side: MarketOrderSide.BUY,
  });

  expect(pricePoint.buyQuantity).toBe(1290);
  expect(pricePoint.sellQuantity).toBe(2110);
});

test('Perform a trade at price point', function () {
  const pricePoint = new PricePoint(222);
  pricePoint.addMarketOrder(marketOrderMock1);
  pricePoint.addMarketOrder(marketOrderMock2);
  pricePoint.addMarketOrder({
    ...marketOrderMock1,
    side: MarketOrderSide.BUY,
    quantity: 56,
  });

  pricePoint.addMarketOrder({
    ...marketOrderMock2,
    sequenceNumber: '675675765',
    side: MarketOrderSide.BUY,
    orderId: '99224834723',
    quantity: 711,
  });

  expect(pricePoint.buyQuantity).toBe(767);
  expect(pricePoint.sellQuantity).toBe(2110);

  pricePoint.performTrade(marketOrderTradeMock);

  expect(pricePoint.buyQuantity).toBe(367);
  expect(pricePoint.sellQuantity).toBe(1710);
  expect(pricePoint).toMatchSnapshot();
});

test('update an Order', function () {
  const pricePoint = new PricePoint(11);
  pricePoint.addMarketOrder(marketOrderMock1);

  expect(pricePoint.getOrder(marketOrderMock1.orderId, MarketOrderSide.SELL)).toEqual(marketOrderMock1);

  pricePoint.updateOrder({
    ...marketOrderMock1,
    quantity: 100,
    sequenceNumber: '445566',
  });

  expect(pricePoint.getOrder(marketOrderMock1.orderId, MarketOrderSide.SELL)).toEqual({
    ...marketOrderMock1,
    quantity: 100,
    sequenceNumber: '445566',
  });
});


# Orderbook aggregator

## Requirements
Only tested for linux, but should work for other platforms too.

* Node JS https://nodejs.org/en/download/ (Any version from the last 5 years)
* npm

## How to install
* Clone project

```git clone https://roger-lab-work@bitbucket.org/roger-lab-work/l3tol1.git```

* Run:

 ```npm install```

* Run: 

 ```npm run build```

## How to Run
* Start service: 

 ```npm start```

The service listens for L3 updates on a websocket at port 9190

The service publishes L1 updates on a websocket at port 9998

For demo purposes a webclient is served at http://localhost:8080 where published L1 data can be viewed

For demo purposes the L1 data is also written as a CSV file by the name of ```L1_log.csv```

* Batch run test data: 

```node build/batch.js l3data.csv```


## How to develop

 * Run tests and check test coverage

  ```npm test```

  Application is written in Typescript.

  The entry point for the service application is ```src/app.ts```

  Most L3 Market updates are processed by O(1) time complexity. 
  Deleting or updating a market order at the best bid or best ask point can be O(n), but this is only under very special circomstances.

  The service works with websocket streams. L3 updates that comes in are sorted on the fly as a stream.
  Sorting streaming data comes with a risk vs speed tradeoff. 
  This application exposes that tradeoff in the form of 2 constants (in constants.ts) that can be adjusted here:
  ```javascript
  export const MAX_OUT_OF_ORDER: number = 50; // Signifies how much out of order a sequence can be  (Higher is safer but also slower)
  export const MAX_WAIT_FOR_NEXT_MESSAGE: number = 500; //ms
  ```



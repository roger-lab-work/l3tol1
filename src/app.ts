import { HTTP_SERVER_PORT, L1_WEBSOCKET_PORT, L3_WEBSOCKET_PORT } from './constants';
import { FileWriter } from './fileWriter';
import { OrderBook } from './orderBook';
import { Listener } from './l3WebSocketServer';
import { L1WebSocketEmitter } from './l1WebSocketEmitter';
import { HttpServer } from './httpServer';
import readline from 'readline';

console.log("Exit application with by hitting 'Q'");
const orderBook = new OrderBook();
const eventToFileHandler = new FileWriter('L1_log.csv');
const eventToWebSocket = new L1WebSocketEmitter(L1_WEBSOCKET_PORT);

orderBook.registerL1Listener(eventToFileHandler.eventHandler);
orderBook.registerL1Listener(eventToWebSocket.eventHandler);

const httpServer = new HttpServer(HTTP_SERVER_PORT);
const socketServer = new Listener(L3_WEBSOCKET_PORT, orderBook);

readline.emitKeypressEvents(process.stdin);
process.stdin.setRawMode(true);
process.stdin.on('keypress', (str, key) => {
  if (key.name.toLowerCase() === 'q') {
    eventToWebSocket.close();
    httpServer.close();
    socketServer.close();
    eventToFileHandler.close();
    console.log('Application ended');
    process.exit(0);
  }
});

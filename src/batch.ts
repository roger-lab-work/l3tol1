import WebSocket from 'ws';
import fs from 'fs';
import readline from 'readline';
import { RawOrder } from './messageMapper';
import { L3_WEBSOCKET_PORT } from './constants';

const infile: string = process.argv[2];

const ws = new WebSocket('ws://localhost:' + L3_WEBSOCKET_PORT);

ws.on('open', function open() {
  console.log('connected to ordebook');

  const reader: readline.Interface = readline.createInterface({
    input: fs.createReadStream(infile),
    output: process.stdout,
    terminal: false,
  });

  reader.on('line', (line) => {
    const args: Array<any> = line.split(',');
    const order: RawOrder = {
      seq_number: args[0],
      add_order_id: args[1],
      add_side: args[2],
      add_price: Number(args[3]),
      add_qty: Number(args[4]),
      update_order_id: args[5],
      update_side: args[6],
      update_price: Number(args[7]),
      update_qty: Number(args[8]),
      delete_order_id: args[9],
      delete_side: args[10],
      trade_order_id: args[11],
      trade_side: args[12],
      trade_price: Number(args[13]) / 1000.0,
      trade_qty: Number(args[14]),
      time: args[15],
    };

    ws.send(JSON.stringify(order));
  });

  reader.on('error', (error) => {
    console.error(error);
  });

  reader.on('close', () => {
    console.log('All data read from file');
    ws.close();
    console.log('All data sent to order book aggregator');
  });
});

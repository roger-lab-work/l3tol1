export const L3_WEBSOCKET_PORT: number = 9190;
export const HTTP_SERVER_PORT: number = 8080;
export const L1_WEBSOCKET_PORT: number = 9998;
export const MAX_OUT_OF_ORDER: number = 50; // Signifies how much out of order a sequence can be  (Higher is safer but also slower)
export const MAX_WAIT_FOR_NEXT_MESSAGE: number = 500; // ms

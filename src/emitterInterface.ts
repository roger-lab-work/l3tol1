export interface L1Data {
  time: string;
  bidPrice: number;
  askPrice: number;
  bidSize: number;
  askSize: number;
  sequenceNumber: string;
}

export type EventHandler = (data: L1Data) => void;

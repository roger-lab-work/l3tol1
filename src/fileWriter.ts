import fs from 'fs';
import { L1Data } from './emitterInterface';

export class FileWriter {
  private writeStream: fs.WriteStream;
  private self: FileWriter;

  constructor(fileName: string) {
    this.writeStream = fs.createWriteStream(fileName);
    this.writeStream.on('finish', () => {
      console.log('Wrote all data to file ' + fileName);
    });
    this.writeStream.write(`time,bid_price,ask_price,bid_size,ask_size,seq_num\n`, 'utf8');
  }

  public eventHandler = (data: L1Data) => {
    const line = `${data.time},${data.bidPrice},${data.askPrice},${data.bidSize},${data.askSize},´${data.sequenceNumber}\n`;
    this.writeStream.write(line, 'utf8');
  };

  public close() {
    this.writeStream.end();
    this.writeStream.close();
  }
}

import express from 'express';
import { readFileSync } from 'fs';

export class HttpServer {
  private application: express.Express;
  private content: string;
  private server;

  constructor(port: number) {
    this.application = express();
    this.content = readFileSync('./ui/index.html').toString();

    this.application.get('/', (req, res) => {
      res.send(this.content);
    });

    this.server = this.application.listen(port, () => {
      console.log(`L1 UI available at http://localhost:${port}`);
    });
  }

  public close = () => {
    this.server.close();
  };
}

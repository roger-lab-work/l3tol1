import WebSocket, { Server } from 'ws';
import { L1Data } from './emitterInterface';

export class L1WebSocketEmitter {
  private wss: WebSocket.Server;
  private connections: Array<WebSocket>;

  constructor(port: number) {
    this.wss = new Server({ port: port });
    this.connections = [];

    this.wss.on('connection', (ws: WebSocket) => {
      console.log(`L1 client connected`);
      this.connections.push(ws);

      ws.on('close', () => {
        ws.close();
        ws.terminate();
        this.connections = this.connections.filter((client: WebSocket) => !client.CLOSED && !client.CLOSING);
        console.log('L1 client disconnected');
      });

      ws.on('message', (message) => {
        console.log('L1 client said:', message.toString());
      });
    });
    console.log(`Listening for L1 client connections on port ${port}`);
  }

  public close = () => {
    this.wss.close();
  };

  public eventHandler = (data: L1Data) => {
    this.connections.forEach((client: WebSocket) => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify(data));
      }
    });
  };
}

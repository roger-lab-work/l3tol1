import WebSocket, { Server } from 'ws';
import { MAX_OUT_OF_ORDER, MAX_WAIT_FOR_NEXT_MESSAGE } from './constants';
import { MarketOrder, MarketOrderType } from './marketOrder';
import { RawOrder, mapToMarketOrder } from './messageMapper';
import { OrderBook } from './orderBook';

export class Listener {
  private wss: WebSocket.Server;
  private orderQueue: Array<MarketOrder>;
  private orderBook: OrderBook;
  private timeout: NodeJS.Timeout;

  constructor(port: number, orderBook: OrderBook) {
    this.wss = new Server({ port: port });
    this.orderBook = orderBook;
    this.timeout = null;
    this.orderQueue = [];

    this.wss.on('connection', (ws: WebSocket) => {
      console.log(`L3 client connected`);
      ws.on('message', (message: string) => {
        const rawOrder: RawOrder = JSON.parse(message);
        const marketOrder: MarketOrder = mapToMarketOrder(rawOrder);
        this.processingTimeout();
        this.orderQueue.push(marketOrder);
        if (this.orderQueue.length >= MAX_OUT_OF_ORDER * 2) {
          this.sortAndProcess(MAX_OUT_OF_ORDER);
        }
      });

      ws.on('close', () => {
        ws.close();
        ws.terminate();
        console.log('L3 client disconnected');
      });
    });
    console.log(`Listening for incomming L3 data on port ${port}`);
  }

  private sortAndProcess = (numberOfOrdersToProcess: number) => {
    let processed: number = 0;
    this.orderQueue.sort((a, b) => (a.sequenceNumber >= b.sequenceNumber ? 1 : -1));
    while (processed <= numberOfOrdersToProcess) {
      const order: MarketOrder = this.orderQueue.shift();
      if (order) {
        this.orderBook.processMarketOrder(order);
      }
      processed++;
    }

    if (this.orderQueue.length > 0) {
      this.processingTimeout();
    }
  };

  public close = () => {
    this.wss.close();
  };

  private processingTimeout = () => {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.sortAndProcess(this.orderQueue.length);
    }, MAX_WAIT_FOR_NEXT_MESSAGE);
  };
}

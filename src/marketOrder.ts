export enum MarketOrderSide {
  BUY = 'BUY',
  SELL = 'SELL',
}

export enum MarketOrderType {
  ADD = 'ADD',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
  TRADE = 'TRADE',
  INVALID = 'INVALID',
}

export interface MarketOrderMap {
  [orderId: number]: MarketOrder;
}

export interface MarketOrder {
  sequenceNumber: string;
  time: string;
  orderType: MarketOrderType;
  orderId?: string;
  side?: MarketOrderSide;
  price?: number;
  quantity?: number;
}

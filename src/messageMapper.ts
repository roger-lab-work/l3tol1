import { MarketOrder, MarketOrderSide, MarketOrderType } from './marketOrder';

export interface RawOrder {
  seq_number: string;
  add_order_id?: string;
  add_side?: string;
  add_price?: number;
  add_qty?: number;
  update_order_id?: string;
  update_side?: string;
  update_price?: number;
  update_qty?: number;
  delete_order_id?: string;
  delete_side?: string;
  trade_order_id?: string;
  trade_side?: string;
  trade_price?: number;
  trade_qty?: number;
  time: string;
}

const getOrderType = (rawOrder: RawOrder): MarketOrderType => {
  if (!!rawOrder.add_order_id) {
    return MarketOrderType.ADD;
  } else if (!!rawOrder.update_order_id) {
    return MarketOrderType.UPDATE;
  } else if (!!rawOrder.delete_order_id) {
    return MarketOrderType.DELETE;
  } else if (!!rawOrder.trade_order_id) {
    return MarketOrderType.TRADE;
  } else {
    return MarketOrderType.INVALID;
  }
};

const getSide = (side: string): MarketOrderSide => {
  if (side === 'SELL') {
    return MarketOrderSide.SELL;
  }
  return MarketOrderSide.BUY;
};

export const mapToMarketOrder = (rawOrder: RawOrder): MarketOrder => {
  const orderType: MarketOrderType = getOrderType(rawOrder);
  const marketOrder: MarketOrder = {
    sequenceNumber: rawOrder.seq_number,
    orderType: orderType,
    time: rawOrder.time,
  };

  switch (orderType) {
    case MarketOrderType.ADD:
      marketOrder.orderId = rawOrder.add_order_id;
      marketOrder.price = rawOrder.add_price;
      marketOrder.quantity = rawOrder.add_qty;
      marketOrder.side = getSide(rawOrder.add_side);
      break;
    case MarketOrderType.UPDATE:
      marketOrder.orderId = rawOrder.update_order_id;
      marketOrder.price = rawOrder.update_price;
      marketOrder.quantity = rawOrder.update_qty;
      marketOrder.side = getSide(rawOrder.update_side);
      break;
    case MarketOrderType.DELETE:
      marketOrder.orderId = rawOrder.delete_order_id;
      marketOrder.side = getSide(rawOrder.delete_side);
      break;
    case MarketOrderType.TRADE:
      marketOrder.orderId = rawOrder.trade_order_id;
      marketOrder.price = rawOrder.trade_price;
      marketOrder.quantity = rawOrder.trade_qty;
      marketOrder.side = getSide(rawOrder.trade_side);
      break;
    default:
      console.log(`Invalid MarketOrderType ${orderType}`);
      break;
  }

  return marketOrder;
};

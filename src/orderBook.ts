import { EventHandler, L1Data } from './emitterInterface';
import { MarketOrder, MarketOrderSide, MarketOrderType } from './marketOrder';
import { PricePoint } from './pricePoint';

interface PricePointMap {
  [price: number]: PricePoint;
}

interface OrderIdMap {
  [orderId: string]: number;
}

export class OrderBook {
  private pricePoints: PricePointMap;
  private orderIdToPrice: OrderIdMap;
  private bestAsk: PricePoint;
  private bestBid: PricePoint;
  private eventListeners: Array<EventHandler>;

  constructor() {
    this.pricePoints = {};
    this.orderIdToPrice = {};
    this.bestAsk = null;
    this.bestBid = null;
    this.eventListeners = [];
  }

  public processMarketOrder(order: MarketOrder) {
    switch (order.orderType) {
      case MarketOrderType.ADD:
        this.addMarketOrder(order);
        break;
      case MarketOrderType.UPDATE:
        this.updateMarketOrder(order);
        break;
      case MarketOrderType.DELETE:
        this.deleteMarketOrder(order);
        break;
      case MarketOrderType.TRADE:
        this.tradeQuantityAtPricePoint(order);
        break;
      default:
        throw Error(`Unknown MarketOrderType ${order.orderType}`);
    }
  }

  public addMarketOrder(order: MarketOrder) {
    const pricePoint = this.getPricePoint(order.price);

    pricePoint.addMarketOrder(order);
    this.pricePoints[pricePoint.price] = pricePoint;
    this.orderIdToPrice[order.orderId] = pricePoint.price;

    if (order.side === MarketOrderSide.BUY) {
      if (this.bestBid === null || order.price >= this.bestBid.price) {
        this.bestBid = pricePoint;
        this.emittL1Event(order);
      }
    } else if (order.side === MarketOrderSide.SELL) {
      if (this.bestAsk === null || order.price <= this.bestAsk.price) {
        this.bestAsk = pricePoint;
        this.emittL1Event(order);
      }
    }
  }

  public deleteMarketOrder(order: MarketOrder) {
    const price: number = this.orderIdToPrice[order.orderId];
    const pricePoint: PricePoint = this.getPricePoint(price);
    const orderDeleted: boolean = pricePoint.deleteMarketOrder(order);

    if (orderDeleted) {
      this.orderIdToPrice[order.orderId] = null;
      if (order.side === MarketOrderSide.BUY && pricePoint.price === this.bestBid.price) {
        this.updateBestBid();
        this.emittL1Event(order);
      } else if (order.side === MarketOrderSide.SELL && pricePoint.price === this.bestAsk.price) {
        this.updateBestAsk();
        this.emittL1Event(order);
      }
    }
  }

  public updateMarketOrder(order: MarketOrder) {
    const price: number = this.orderIdToPrice[order.orderId];
    const pricePoint: PricePoint = this.getPricePoint(price);
    const priceUnchanged: boolean = price === order.price;

    if (priceUnchanged) {
      const bestAskQuantityBeforeChange = this.bestAsk.sellQuantity;
      const bestBidQuantityBeforeChange = this.bestBid.buyQuantity;

      pricePoint.updateOrder(order);

      const bestAskQuantityAfterChange = this.bestAsk.sellQuantity;
      const bestBidQuantityAfterChange = this.bestBid.buyQuantity;

      if (
        bestAskQuantityBeforeChange !== bestAskQuantityAfterChange ||
        bestBidQuantityBeforeChange !== bestBidQuantityAfterChange
      ) {
        this.emittL1Event(order);
      }
    } else {
      pricePoint.deleteMarketOrder(order);
      const newPricePoint: PricePoint = this.getPricePoint(order.price);
      newPricePoint.addMarketOrder(order);
      this.pricePoints[pricePoint.price] = newPricePoint;
      this.orderIdToPrice[order.orderId] = newPricePoint.price;

      if (pricePoint.price === this.bestAsk.price || newPricePoint.price <= this.bestAsk.price) {
        this.updateBestAsk();
        this.emittL1Event(order);
      }

      if (pricePoint.price === this.bestBid.price || newPricePoint.price >= this.bestBid.price) {
        this.updateBestBid();
        this.emittL1Event(order);
      }
    }
  }

  public tradeQuantityAtPricePoint(order: MarketOrder) {
    const pricePoint: PricePoint = this.getPricePoint(order.price);
    const tradeIsHappeningAtBBO = order.price === this.bestAsk.price || order.price === this.bestBid.price;
    pricePoint.performTrade(order);

    if (this.bestBid.sellQuantity === 0) {
      this.updateBestBid();
    }

    if (this.bestAsk.sellQuantity === 0) {
      this.updateBestAsk();
    }
    this.emittL1Event(order);
  }

  public registerL1Listener(handler: EventHandler) {
    this.eventListeners.push(handler);
  }

  private updateBestBid() {
    if (this.bestBid.buyQuantity === 0) {
      const allPrices: Array<string> = Object.keys(this.pricePoints);
      const allPricesWithBuyQuantity: Array<string> = allPrices.filter((price) => {
        const pricePoint: PricePoint = this.pricePoints[Number(price)];
        if (pricePoint && pricePoint.buyQuantity !== 0) {
          return true;
        }
        return false;
      });

      if (allPricesWithBuyQuantity.length === 0) {
        this.bestBid = null;
      } else {
        allPricesWithBuyQuantity.sort((a, b) => Number(b) - Number(a));
        this.bestBid = this.pricePoints[Number(allPricesWithBuyQuantity[0])];
      }
    }
  }

  private updateBestAsk() {
    if (this.bestAsk.sellQuantity === 0) {
      const allPrices: Array<string> = Object.keys(this.pricePoints);
      const allPricesWithSellQuantity: Array<string> = allPrices.filter((price) => {
        const pricePoint: PricePoint = this.pricePoints[Number(price)];
        if (pricePoint && pricePoint.sellQuantity !== 0) {
          return true;
        }
        return false;
      });

      if (allPricesWithSellQuantity.length === 0) {
        this.bestAsk = null;
      } else {
        allPricesWithSellQuantity.sort((a, b) => Number(a) - Number(b));
        this.bestAsk = this.pricePoints[Number(allPricesWithSellQuantity[0])];
      }
    }
  }

  private getPricePoint(price: number): PricePoint {
    return this.pricePoints[price] ? this.pricePoints[price] : new PricePoint(price);
  }

  private emittL1Event(order: MarketOrder) {
    if (this.bestAsk && this.bestBid) {
      const l1Data: L1Data = {
        time: order.time,
        bidPrice: this.bestBid.price,
        askPrice: this.bestAsk.price,
        bidSize: this.bestBid.buyQuantity,
        askSize: this.bestAsk.sellQuantity,
        sequenceNumber: order.sequenceNumber,
      };

      this.eventListeners.forEach((handler) => handler(l1Data));
    }
  }
}

import { MarketOrder, MarketOrderSide } from './marketOrder';

interface Orders {
  [orderId: string]: MarketOrder;
}

export class PricePoint {
  public price: number;
  public buy: Orders;
  public sell: Orders;

  constructor(price: number) {
    this.price = price;
    this.buy = {};
    this.sell = {};
  }

  public get buyQuantity(): number {
    return this.getSideOrderQuantity(this.buy);
  }

  public get sellQuantity(): number {
    return this.getSideOrderQuantity(this.sell);
  }

  public getOrder(orderId: string, side: MarketOrderSide): MarketOrder {
    const orders = side === MarketOrderSide.BUY ? this.buy : this.sell;
    return orders[orderId];
  }

  private getSideOrderQuantity(orders: Orders): number {
    let orderQuantity = 0;

    Object.keys(orders).forEach((orderId) => {
      const order = orders[orderId];
      if (order) {
        orderQuantity += order.quantity;
      }
    });

    return orderQuantity;
  }

  public addMarketOrder(order: MarketOrder): void {
    if (order.side === MarketOrderSide.SELL) {
      this.sell[order.orderId] = order;
    } else if (order.side === MarketOrderSide.BUY) {
      this.buy[order.orderId] = order;
    }
  }

  public deleteMarketOrder(deleteOrder: MarketOrder): boolean {
    let changeWasMade: boolean = false;
    const orders = deleteOrder.side === MarketOrderSide.BUY ? this.buy : this.sell;

    if (orders[deleteOrder.orderId]) {
      orders[deleteOrder.orderId] = null;
      changeWasMade = true;
    }

    return changeWasMade;
  }

  public performTrade(order: MarketOrder) {
    let remainingQuantity: number = order.quantity;
    const sellOrders: Array<MarketOrder> = Object.keys(this.sell)
      .map((orderId) => this.sell[Number(orderId)])
      .filter((order) => order && order.quantity !== 0);

    const buyOrders: Array<MarketOrder> = Object.keys(this.buy)
      .map((orderId) => this.buy[orderId])
      .filter((order) => order && order.quantity !== 0);

    sellOrders.sort((a, b) => (a.sequenceNumber >= b.sequenceNumber ? 1 : -1));
    buyOrders.sort((a, b) => (a.sequenceNumber >= b.sequenceNumber ? 1 : -1));

    sellOrders.forEach((sellOrder) => {
      buyOrders.forEach((buyOrder) => {
        if (
          remainingQuantity > 0 &&
          ((order.orderId === sellOrder.orderId && order.side === sellOrder.side) ||
            (order.orderId === buyOrder.orderId && order.side === buyOrder.side))
        ) {
          const executionSize: number = Math.min(remainingQuantity, sellOrder.quantity, buyOrder.quantity);
          sellOrder.quantity -= executionSize;
          buyOrder.quantity -= executionSize;
          remainingQuantity -= executionSize;
        }
      });
    });
  }

  public updateOrder(order: MarketOrder): void {
    this.deleteMarketOrder(order);
    this.addMarketOrder(order);
  }
}
